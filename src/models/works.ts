export interface Work {
  id: string | number;
  title: string;
  tag: string[];
  createdAt: string;
  shortDescription: string;
  fullDescription: string;
  image: string;
}
