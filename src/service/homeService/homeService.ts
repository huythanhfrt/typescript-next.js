import { Post, Work } from "@/models";
export const postList: Array<Post> = [
  {
    id: 1,
    slug: "",
    title: "Making a design system from scratch",
    date: "1674268819594",
    tag: ["React", "Vue"],
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident saepe impedit, dolorem incidunt obcaecati voluptates!",
  },
  {
    id: 2,
    slug: "",
    title: "Making a design system from scratch",
    date: "1674268819594",
    tag: ["Prisma", "Sequelize"],
    description:
      " Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident saepe impedit, dolorem incidunt obcaecati voluptates!",
  },
];
export const workList: Array<Work> = [
  {
    id: 1,
    title: "Designing Dashboards",
    tag: ["Dashboard"],
    createdAt: "2018",
    shortDescription:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo quia blanditiis, hic explicabo obcaecati sit. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo quia blanditiis, hic explicabo obcaecati sit.",
    fullDescription:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo quia blanditiis, hic explicabo obcaecati sit.",
    image:
      "https://media.istockphoto.com/id/522375214/fr/photo/la-ville-de-shanghai.jpg?b=1&s=170667a&w=0&k=20&c=HcD06wcg55raXzdSgORg08Ypv5ssQZMQWEPyCUxz4qM=",
  },
  {
    id: 2,
    title: "Vibrant Portraits of 2022",
    tag: ["Illustration"],
    createdAt: "2018",
    shortDescription:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo quia blanditiis, hic explicabo obcaecati sit. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo quia blanditiis, hic explicabo obcaecati sit.",
    fullDescription:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo quia blanditiis, hic explicabo obcaecati sit.",
    image:
      "https://media.istockphoto.com/id/599494684/fr/photo/shanghai-skyline-panoramique-au-coucher-du-soleil.jpg?b=1&s=170667a&w=0&k=20&c=IZkArugI0LDvJl-2Q2klA5GVP77KYAukgRwGE6GOWR8=",
  },
  {
    id: 3,
    title: "36 Days of Malayalam type",
    tag: ["Typography"],
    createdAt: "2018",
    shortDescription:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo quia blanditiis, hic explicabo obcaecati sit. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo quia blanditiis, hic explicabo obcaecati sit.",
    fullDescription:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo quia blanditiis, hic explicabo obcaecati sit.",
    image:
      "https://media.istockphoto.com/id/1179978682/fr/photo/chine-shanghai-lujiazui.jpg?b=1&s=170667a&w=0&k=20&c=eR_T72aGmaGtpHIGvwESMuEhBP-WmsixxmyDudbf-WQ=",
  },
];
