import { Typography } from "@mui/material";
import { Roboto } from "@next/font/google";
import { createTheme, responsiveFontSizes } from "@mui/material/styles";
import { red } from "@mui/material/colors";

export const roboto = Roboto({
  weight: ["300", "400", "500", "700"],
  subsets: ["latin"],
  display: "swap",
  fallback: ["Helvetica", "Arial", "sans-serif"],
});

// Create a theme instance.
export let theme = createTheme({
  palette: {
    primary: {
      main: "#F55050",
    },
    secondary: {
      main: "#19857b",
      light: "#789b",
    },
    error: {
      main: red.A400,
    },
  },
  typography: {
    fontFamily: "Heebo, sans-serif",

    // fontFamily: roboto.style.fontFamily,
  },
  components: {
    MuiTypography: {
      defaultProps: {
        fontFamily: '"Heebo", sans-serif',
      },
    },
    MuiContainer: {
      defaultProps: {
        maxWidth: "md",
      },
      styleOverrides: {
        maxWidthSm: {
          maxWidth: "680px",
          "@media (min-width:600px)": {
            maxWidth: "680px",
          },
          padding: "0",
        },
        maxWidthMd: {
          maxWidth: "860px",
          "@media (min-width:900px)": {
            maxWidth: "860px",
            padding: "0",
          },
        },
      },
    },
  },
});
// theme.typography.h3 = {
//   fontSize: "2rem",
//   [theme.breakpoints.up("md")]: {
//     fontSize: "3rem",
//   },
// };
theme = responsiveFontSizes(theme);
// responsive fontsize https://mui.com/material-ui/customization/typography/#responsive-font-sizes
