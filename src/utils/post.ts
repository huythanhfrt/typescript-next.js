import path from "path";
import fs from "fs";
import matter from "gray-matter";
import { Post } from "@/models";
export const getPostList = (): Post[] => {
  const pathName = path.join(process.cwd(), "/src/markdown");
  const pathList = fs.readdirSync(pathName);
  const postList: Post[] = [];
  for (let i = 0; i < pathList.length; i++) {
    let filePath = pathList[i];
    const file = path.join(pathName, filePath);
    let contentFile = fs.readFileSync(file, "utf8");
    const { data, excerpt, content } = matter(contentFile, {
      excerpt_separator: "<!-- truncate-->", // sau phần đề mục
    });
    postList.push({
      id: filePath,
      slug: data.slug,
      title: data.title,
      author: {
        name: data.author,
        title: data.author_title,
        profileUrl: data.author_url,
        avatarUrl: data.author_image_url,
      },
      tag: data.tags,
      description: excerpt || "",
      date: new Date().getTime().toString(),
      mdContent: content,
      thumbnailUrl: data.image || null,
    });
  }
  return postList;
};
