import { FeatureWork, HeroSection, RecentPost } from "@/components/home";
import { MainLayout } from "@/components/layout/main";
import { Seo } from "@/components/seo";
import { Box } from "@mui/material";
export default function HomePage() {
  return (
    <Box>
      <Seo
        data={{
          title: "Typescript - Next.js",
          desc: "Learn Next.js by Typescript languages",
          url: "https://learn-nextjs-fawn.vercel.app",
          thumbnailUrl:
            "https://miro.medium.com/max/1042/1*9mESIE8IL4eEFZ6FIO4smA.png",
        }}
      />
      <HeroSection />
      <RecentPost />
      <FeatureWork />
    </Box>
  );
}
HomePage.Layout = MainLayout;
