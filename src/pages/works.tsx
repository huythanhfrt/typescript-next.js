import { MainLayout } from "@/components/layout/main";
import { Box } from "@mui/material";
import * as React from "react";

export interface WorkPageProps {}

export default function WorkPage(props: WorkPageProps) {
  return <Box>WorkPage</Box>;
}
WorkPage.Layout = MainLayout;
