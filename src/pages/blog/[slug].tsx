import { Seo } from "@/components/seo";
import { Post } from "@/models";
import { getPostList } from "@/utils/post";
import { Box, Container } from "@mui/material";
import { GetStaticPaths, GetStaticProps, GetStaticPropsContext } from "next";
import Script from "next/script";
import rehypeAutolinkHeadings from "rehype-autolink-headings";
import rehypeDocument from "rehype-document";
import rehypeFormat from "rehype-format";
import rehypeSlug from "rehype-slug";
import rehypeStringify from "rehype-stringify";
import remarkParse from "remark-parse/lib";
import remarkPrism from "remark-prism";
import remarkRehype from "remark-rehype";
import remarkToc from "remark-toc";
import { unified } from "unified";
export interface BlogDetailPageProps {
  post: Post;
}
export default function BlogDetailPage({ post }: BlogDetailPageProps) {
  if (!post) return null;
  return (
    <Box>
      <Seo
        data={{
          title: post.title,
          desc: post.description,
          url: `https://demo/blog/${post.slug}`,
          thumbnailUrl:
            post.thumbnailUrl ||
            "https://miro.medium.com/max/1042/1*9mESIE8IL4eEFZ6FIO4smA.png",
        }}
      />
      <Container>
        <h1>Post Detail Page</h1>
        <p>{post.title}</p>
        <p>{post.author?.name}</p>
        <p>{post.description}</p>
        <div dangerouslySetInnerHTML={{ __html: post.htmlContent || "" }}></div>
      </Container>
      <Script src="../../utils/prism.js" strategy="afterInteractive"></Script>
    </Box>
  );
}
export const getStaticPaths: GetStaticPaths = async () => {
  const postList = await getPostList();
  return {
    paths: postList.map((post: Post) => ({ params: { slug: post.slug } })),
    //tên file là gì thì lấy đó làm params
    fallback: false,
  };
};
export const getStaticProps: GetStaticProps<BlogDetailPageProps> = async (
  context: GetStaticPropsContext
) => {
  const postList = await getPostList();
  const slug = context.params?.slug;
  if (!slug) return { notFound: true };
  const post = postList.find((x) => x.slug === slug);
  if (!post) return { notFound: true };
  const file = await unified()
    .use(remarkParse)
    .use(remarkPrism, { plugins: ["line-numbers"] }) // css , js cho content
    .use(remarkToc, { heading: "agenda.*" }) // tạo table of header chuyển đến head nhanh
    .use(remarkRehype) // kết nối
    .use(rehypeSlug) // gắn id vào header
    .use(rehypeAutolinkHeadings, { behavior: "wrap" }) // gắn id vào url khi click vào header
    .use(rehypeDocument, { title: "Blog NextJs" })
    .use(rehypeFormat)
    .use(rehypeStringify)
    .process(post.mdContent || "");
  post.htmlContent = file.toString();
  return {
    props: {
      post: post,
    },
  };
};
