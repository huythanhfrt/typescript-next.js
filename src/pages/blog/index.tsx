import { PostComponent } from "@/components/blog/blog";
import { MainLayout } from "@/components/layout/main";
import { getPostList } from "@/utils/post";
import { Box, Container, Divider } from "@mui/material";
import { GetStaticProps, GetStaticPropsContext } from "next";
import Link from "next/link";

export interface BlogPageProps {
  post: any[];
}

export default function BlogPage({ post }: BlogPageProps) {
  return (
    <Box>
      <Container>
        <h1>Blog</h1>
        <Box component="ul" sx={{ listStyleType: "none", p: 0, m: 0 }}>
          {post.map((post) => {
            return (
              <li key={post.id}>
                <Link style={{ color: "black" }} href={`/blog/${post.slug}`}>
                  <PostComponent post={post} />
                </Link>
                <Divider sx={{ my: 5 }} />
              </li>
            );
          })}
        </Box>
      </Container>
    </Box>
  );
}
BlogPage.Layout = MainLayout;
export const getStaticProps: GetStaticProps<BlogPageProps> = async (
  context: GetStaticPropsContext
) => {
  const data = await getPostList();
  return {
    props: {
      post: data,
    },
  };
};
