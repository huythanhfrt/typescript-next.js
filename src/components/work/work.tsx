import { Box, Divider, Stack, Typography } from "@mui/material";
import Image from "next/image";
import * as React from "react";
import { Work } from "@/models";

export interface WorkListProps {
  work: Work;
}
export function WorkList({ work }: WorkListProps) {
  return (
    <Box mt={5}>
      <Stack spacing={3} direction={{ xs: "column", md: "row" }}>
        <Image
          width={300}
          height={170}
          style={{ borderRadius: "10px" }}
          src={work.image}
          alt="avatar"
        />
        <Box>
          <Typography color="primary.main" variant="h4">
            {work.title}
          </Typography>
          <Stack gap={5} my={2} color="white" direction="row">
            <Box
              component="div"
              sx={{
                borderRadius: "15px",
                padding: "2px 25px",
                bgcolor: "primary.main",
                textAlign: "center",
              }}
            >
              <Typography variant="h6" fontSize="16px">
                {work.createdAt}
              </Typography>
            </Box>
            <Typography fontSize="16px" color="secondary.light" variant="h6">
              {work.tag}
            </Typography>
          </Stack>
          <Typography>{work.shortDescription}</Typography>
        </Box>
      </Stack>
      <Divider style={{ margin: "25px 0" }} />
    </Box>
  );
}
