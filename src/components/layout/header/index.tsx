import * as React from "react";
import { HeaderDesktop } from "./HeaderDesktop";
import { HeaderMobile } from "./headerMobile";

export function Header() {
  return (
    <>
      <HeaderDesktop />
      <HeaderMobile />
    </>
  );
}
