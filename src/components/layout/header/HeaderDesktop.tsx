import { Box, Link as MuiLink } from "@mui/material";
import { Container, Stack } from "@mui/system";
import * as React from "react";
import { ROUTE_LINK } from "./routes";
import Link from "next/link";
import clsx from "clsx";
import { useRouter } from "next/router";
export function HeaderDesktop() {
  const router = useRouter();
  return (
    <Box
      display={{ xs: "none", md: "block" }}
      component="header"
      textAlign="center"
      py={2}
    >
      <Container>
        <Stack justifyContent="flex-end" gap={1} direction="row">
          {ROUTE_LINK.map((route) => (
            <Link
              className={
                route.path === router.pathname
                  ? "active header-route"
                  : "header-route"
              }
              key={route.path}
              href={route.path}
            >
              {route.label}
            </Link>
          ))}
        </Stack>
      </Container>
    </Box>
  );
}
