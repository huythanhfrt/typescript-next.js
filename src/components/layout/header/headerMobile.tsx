import { Box } from "@mui/material";
import * as React from "react";

export function HeaderMobile() {
  return (
    <Box
      display={{ xs: "block", md: "none" }}
      component="header"
      textAlign="center"
      py={2}
    >
      HeaderMobile
    </Box>
  );
}
