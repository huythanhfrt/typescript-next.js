export const ROUTE_LINK = [
  {
    label: "Home",
    path: "/",
  },
  {
    label: "Blog",
    path: "/blog",
  },
  {
    label: "Works",
    path: "/works",
  },
];
