import { LayoutProps } from "@/models";
import { Box } from "@mui/material";
import { Stack } from "@mui/system";
import * as React from "react";
import { Footer } from "./Footer";
import Container from "@mui/material/Container";
import { Header } from "./header";

export interface MainLayoutProps {}

export function MainLayout({ children }: LayoutProps) {
  return (
    <Stack minHeight="100vh">
      <Header />
      <Box component="main" flexGrow={1}>
        {children}
        {/* <Container maxWidth="md">{children}</Container> */}
      </Box>
      <Footer />
    </Stack>
  );
}
