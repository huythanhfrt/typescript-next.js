import { Post } from "@/models";
import { Box } from "@mui/material";
import { Typography, Stack, Divider } from "@mui/material";
import { format } from "date-fns";
import * as React from "react";

export interface PostComponentProps {
  post: Post;
}

export function PostComponent({ post }: PostComponentProps) {
  console.log("post: ", post);
  return (
    <Box>
      <Typography fontWeight="bold" variant="h4">
        {post.title}
      </Typography>
      <Stack direction="row">
        <Typography>{format(Number(post.date), " MMM dd yyyy")}</Typography>
        <Divider sx={{ m: 2 }} orientation="vertical" flexItem />
        <Typography>{post.tag.join(" , ")}</Typography>
      </Stack>
      <Typography>{post.description}</Typography>
    </Box>
  );
}
