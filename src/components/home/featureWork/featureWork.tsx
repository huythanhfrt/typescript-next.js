import { WorkList } from "@/components/work";
import { workList } from "@/service/homeService/homeService";
import { Box, Container, Typography } from "@mui/material";
import * as React from "react";

export function FeatureWork() {
  return (
    <Box my={3} component="section">
      <Container>
        <Typography fontWeight="light" variant="h6">
          Featured works
        </Typography>
        {workList.map((work) => (
          <WorkList key={work.id} work={work} />
        ))}
      </Container>
    </Box>
  );
}
