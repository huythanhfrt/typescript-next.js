import { Box } from "@mui/system";
import React from "react";
import { Container, Stack, Typography } from "@mui/material";
import Image from "next/image";
import Button from "@mui/material/Button";

export function HeroSection() {
  return (
    <Box component="section" pt={{ xs: 4, md: 18 }} pb={{ xs: 4, md: 9 }}>
      <Container>
        <Stack
          spacing={4}
          alignItems={{ xs: "center", md: "flex-start" }}
          direction={{ xs: "column-reverse", md: "row" }}
          textAlign={{ xs: "center", md: "left" }}
        >
          <Box>
            <Typography component="h1" variant="h3">
              Hi , I am John ,<br />
              Creative Technology
            </Typography>
            <Typography sx={{ padding: "40px 0" }}>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Exercitationem quod vel vero iusto, quam dolor quos dicta repellat
              dolorum inventore, consequatur voluptatem, quo itaque
            </Typography>
            <Button
              disableElevation
              sx={{ width: "280px", height: "40px" }}
              variant="contained"
            >
              Download resume
            </Button>
          </Box>
          <Box
            boxShadow="-5px 13px #F55050"
            borderRadius="50%"
            minWidth={250}
            height={250}
          >
            <Image
              style={{ borderRadius: "50%" }}
              width={250}
              height={250}
              src="https://vanhoadoisong.vn/wp-content/uploads/2022/08/tong-hop-100-avatar-dep-cute-de-thuong-cho-cac-cap-doi-09.jpg"
              alt="avatar"
            />
          </Box>
        </Stack>
      </Container>
    </Box>
  );
}
