import { Post } from "@/models";
import { Divider, Typography } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { Stack } from "@mui/system";
import React from "react";
import { format } from "date-fns";
import { PostComponent } from "@/components/blog/blog";
export interface CardProps {
  post: Post;
}
export function CardComponent({ post }: CardProps) {
  return (
    <Card>
      <CardContent>
        <PostComponent post={post} />
      </CardContent>
    </Card>
  );
}
