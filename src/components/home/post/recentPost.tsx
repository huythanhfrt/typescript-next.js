import { Box, Typography, Link as MuiLink } from "@mui/material";
import Container from "@mui/material/Container";
import Stack from "@mui/material/Stack";
import Link from "next/link";
import React from "react";
import { CardComponent } from "./card";
import { useWindowSize } from "usehooks-ts";
import { postList } from "@/service/homeService/homeService";

export function RecentPost() {
  const { width, height } = useWindowSize();

  return (
    <Box component="section" bgcolor="#F55050" pt={2} pb={4}>
      <Container>
        <Stack
          alignItems="center"
          direction="row"
          mb={2}
          justifyContent={{ xs: "center", md: "space-between" }}
        >
          <Typography variant="h5">Recent Posts</Typography>
          <Link
            style={{ color: "black", textDecoration: "none" }}
            passHref
            href="/blog"
          >
            {width > 860 ? " View All" : ""}
          </Link>
        </Stack>
        <Stack
          direction={{ xs: "column", md: "row" }}
          sx={{
            "& > div": {
              width: {
                xs: "100%",
                md: "50%",
              },
            },
          }}
          spacing={3}
        >
          {postList.map((post) => (
            <Box key={post.id}>
              <CardComponent post={post} />
            </Box>
          ))}
        </Stack>
      </Container>
    </Box>
  );
}
